#FROM python:3.8-slim-buster
FROM python:buster
WORKDIR /src
COPY requirements.txt .
RUN pip install -r requirements.txt
RUN apt-get update
RUN apt-get install -y  vim
COPY ./src/server.py .

CMD ["python3", "./server.py"]
