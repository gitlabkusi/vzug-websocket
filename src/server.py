import asyncio
import websockets
import time
from vzug.vzug import VZUG
import json
import socket

IP_ADDRESS = "adora"
USERNAME = "YOUR_USERNAME"
PASSWORD = "YOUR_PASSWORD"
ITER = 0


def get_remaining_minutes(d):
    time_str = d['ProgramEnd']['End']
    t = time.strptime(time_str.split(',')[0], '%H:%M')
    return t.tm_hour*60 + t.tm_min


def state_machine(state, data):
    if (state == "Idle") and (data['Inactive'] == "false"):
        state = "Start"
    elif (state == "Start") and (get_remaining_minutes(data) < 5):
        state = "FiveMinutesRemaining"
    elif state == "FiveMinutesRemaining" and data['Inactive'] == 'true':
        state = "Idle"
    else:
        return state, False

    return state, True


def mocked_vzug_data():
    global ITER
    # in order print to work, you need a node-red socket connected to the port
    data = [
        '{"DeviceName":"","Serial":"11013 000098","Inactive":"true","Program":"","Status":"","ProgramEnd":{"End":"","EndType":"0"}}',
        '{"DeviceName":"","Serial":"11013 000098","Inactive":"false","Program":"40°C Buntwäsche","Status":"Bla","ProgramEnd":{"End":"1:17","EndType":"2"}}',
        '{"DeviceName":"","Serial":"11013 000098","Inactive":"false","Program":"40°C Buntwäsche","Status":"Auflockern (Türöffnung möglich)","ProgramEnd":{"End":"0:03","EndType":"2"}}'
    ]

    ITER += 1

    if ITER < 3:
        idx = 0
    elif ITER < 6:
        idx = 1
    elif ITER < 9:
        idx = 2
    else:
        idx = 0
        ITER = 0

    return json.loads(data[idx])


async def poll_vzug(websocket, path):

    state = "Idle"

    while True:
        # print('polling VZUG device', flush=True)
        await asyncio.sleep(20)
        data = await get_vzug_data()
        # data = mocked_vzug_data()
        (state, state_changed) = state_machine(state, data)
        if state_changed:
            print(f"send {state}", flush=True)
            s = json.dumps({'state': state})
            await websocket.send(s)


async def get_vzug_data():
    async with VZUG(IP_ADDRESS, USERNAME, PASSWORD) as vzug:
        await vzug.get_device_status()
        return json.loads(vzug.device_status)


async def status_query(websocket, path):
    while True:
        print('before receive', flush=True)
        await websocket.recv()
        data = await get_vzug_data()
        print('after receive', flush=True)
        print(data, flush=True)

        if data['Inactive'] == 'true':
            answer = {'active': False}
        else:
            answer = {'active': True, 'remainingMinutes': get_remaining_minutes(data)}

        await websocket.send(json.dumps(answer))


def main():
    host_name = socket.gethostname()
    # host_name = 'vzug'
    # host_name = 'localhost'
    port_send = 6677
    port_query = 6678

    # Create websocket server
    start_server_send = websockets.serve(poll_vzug, host_name, port_send)
    start_server_recv = websockets.serve(status_query, host_name, port_query)

    # Start and run websocket server forever
    asyncio.get_event_loop().run_until_complete(start_server_send)
    asyncio.get_event_loop().run_until_complete(start_server_recv)
    print(f"server started on {host_name}:{port_send}", flush=True)
    asyncio.get_event_loop().run_forever()


main()
